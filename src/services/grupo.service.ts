import { Grupo, GrupoXML } from './../models/grupo.model';
import { AxiosError, AxiosInstance } from "axios";
import { parse, ValidationError } from 'fast-xml-parser'

export class GrupoService {
    constructor(
        private client: AxiosInstance
    ) {}

    public async getGrupos(agenda: number, idade: number, unidade: number) {
        try {
            const { data } = await this.client.get(`/passo04/itens-agenda.xml.asp?cabxml=s&agenda=${agenda}&idade=${idade}&unidade=${unidade}`)
            const root = parse(data, {ignoreAttributes: false, parseAttributeValue: true});
            return (root.retorno.itensAgenda as GrupoXML[]).map(g => new Grupo(g, agenda))
        } catch(error) {
            if (error.isAxiosError) {
                const _error = error as AxiosError;
                process.stderr.write(`Falha na requisição [${_error.config.url}]: ${_error.code} ${_error.message}`+'\n');
            } else if (error.err) {
                const _error = error as ValidationError;
                process.stderr.write(`Falha no processamento de dados [${_error.err.line}]: ${_error.err.code} ${_error.err.msg}`+'\n');
            } else {
                process.stderr.write(`Falha inesperada: ${JSON.stringify(error)}`+'\n');
            }
        }
    }
}