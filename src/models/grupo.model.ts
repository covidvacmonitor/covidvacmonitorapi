import { Disponibilidade } from './disponibilidade.model';

export interface GrupoXML {
    '@_numero': number, 
    '@_texto': string
}

export enum Tipo {
    IDOSOACIMA60 = 0, //26
    IDOSO60A65 = 1,
    IDOSOACIMA70 = 2, //27
    IDOSOACIMA65 = 3,
    IDOSOACIMA75 = 4, //25
    IDOSOACIMA80 = 5,
    TRABALHADORSAUDE = 6, //8
    NAOIDENTIFICADO = 7
}

export enum Dose {
    primeira = 1,
    segunda = 3
}

export class Grupo extends Disponibilidade {
    public texto: string;
    public value: number;
    public tipo: Tipo;
    public dose: Dose;

    constructor(value: GrupoXML, agenda: number) {
        super(value['@_texto'])
        this.texto = this.cleanGrupo(value['@_texto']);
        this.value = value['@_numero'];
        this.tipo = this.getTipo();
        this.dose = agenda;
    }

    private cleanGrupo(grupo: string) {
        return grupo.replace(/-\s+\d[^\x00-\x7F]+\s+dose/, '').trim();
    }

    private getTipo() {
        const regIdosoAcima60 = /.*acima de 60.*/;
        const regIdoso60a65 = /.*de 60 a 65.*/
        const regIdosoAcima65 = /.*acima de 65.*/;
        const regIdosoAcima70 = /.*acima de 70.*/;
        const regIdosoAcima75 = /.*acima de 75.*/;
        const regIdosoAcima80 = /.*acima de 85.*/;
        const regTrabalhadorSaude = /.*[Tt]rabalhador\s+de\s+[Ss]a[^\x00-\x7F]+de.*/;

        if(regIdosoAcima60.test(this.texto)) { return Tipo.IDOSOACIMA60 }
        if(regIdoso60a65.test(this.texto)) { return Tipo.IDOSO60A65 }
        if(regIdosoAcima65.test(this.texto)) { return Tipo.IDOSOACIMA65 }
        if(regIdosoAcima70.test(this.texto)) { return Tipo.IDOSOACIMA70 }
        if(regIdosoAcima75.test(this.texto)) { return Tipo.IDOSOACIMA75 }
        if(regIdosoAcima80.test(this.texto)) { return Tipo.IDOSOACIMA80 }
        if(regTrabalhadorSaude.test(this.texto)) { return Tipo.TRABALHADORSAUDE }

        return Tipo.NAOIDENTIFICADO;
    }
}