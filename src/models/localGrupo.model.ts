import { Grupo } from './grupo.model';
import { Local } from './local.model';

export class LocalGrupo {
    public local: Local;
    public grupo: Grupo;

    constructor(local: Local, grupo: Grupo) {
        this.local = local;
        this.grupo = grupo
    }
}