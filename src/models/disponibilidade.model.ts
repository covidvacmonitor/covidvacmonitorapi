export class Disponibilidade {
    public disponivel: boolean;

    constructor(texto: string) {
        this.disponivel = !(/.*([aA]guardando disponibilidade de doses)/.test(texto));
    }
}